import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import createStyles from "@material-ui/core/styles/createStyles";
import Box from "@material-ui/core/Box";

export default function Background({ children }) {
  const useStyles = makeStyles((theme) =>
    createStyles({
      root: {
        backgroundColor: theme.palette.background.main,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center",
        minHeight: "100vh",
      },
    })
  );

  const classes = useStyles();
  return <Box className={classes.root}>{children}</Box>;
}
