import React from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import ChevronRight from "@material-ui/icons/ChevronRight";
import { useHistory } from "react-router";

export default function RedirectButton({ title, to }) {
  const history = useHistory();
  const handleClick = () => {
    history.push(to);
  };
  return (
    <Button
      fullWidth
      variant="outlined"
      color="secondary"
      onClick={handleClick}
    >
      <Box
        display="flex"
        justifyContent="space-between"
        width="100%"
        paddingLeft="0.5rem"
      >
        <Typography>{title}</Typography>
        <ChevronRight />
      </Box>
    </Button>
  );
}
