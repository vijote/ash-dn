import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import { createMuiTheme } from "@material-ui/core";

export default function Footer() {
  const theme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.footerBackground.main,
        },
      },
    });
  return (
    <ThemeProvider theme={theme}>
      <AppBar position="static">
        <Toolbar>
          <Typography>
            Website made by{" "}
            <Link
              href="https://www.linkedin.com/in/judom/"
              target="_blank"
              rel="noopener"
              color="secondary"
            >
              Juan
            </Link>
          </Typography>
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
}
