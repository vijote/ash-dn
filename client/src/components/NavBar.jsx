import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuIcon from "@material-ui/icons/Menu";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import { createMuiTheme } from "@material-ui/core";
import useTheme from "@material-ui/core/styles/useTheme";
import { useHistory } from "react-router-dom";

export default function NavBar() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (to) => () => {
    setAnchorEl(null);
    history.push(to);
  };
  const history = useHistory();
  const theme = useTheme();
  const navbarTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.navbarBackground.main,
        },
      },
      overrides: {
        MuiPaper: {
          root: {
            backgroundColor: theme.palette.navbarBackground.main,
          },
        },
      },
    });
  const matches = useMediaQuery(theme.breakpoints.up("md"));

  return (
    <ThemeProvider theme={navbarTheme}>
      <AppBar position="static" color="primary">
        <Toolbar>
          <Grid container alignItems="center" justify="space-between">
            <Typography
              style={{ cursor: "pointer" }}
              onClick={() => history.push("/")}
            >
              Agrupacion Super Heroes
            </Typography>
            {matches ? (
              <Box>
                <Button
                  onClick={() => history.push("/nuevo-heroe")}
                  variant="text"
                  color="secondary"
                >
                  Nuevo Heroe
                </Button>
                <Button
                  onClick={() => history.push("/registrar-mision")}
                  variant="text"
                  color="secondary"
                >
                  Registrar Mision
                </Button>
                <Button
                  onClick={() => history.push("/nuevo-poder")}
                  variant="text"
                  color="secondary"
                >
                  Nuevo Poder
                </Button>
                <Button
                  onClick={() => history.push("/registrar-planeta")}
                  variant="text"
                  color="secondary"
                >
                  Registrar Planeta
                </Button>
              </Box>
            ) : (
              <IconButton
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
                edge="end"
                // className={classes.menuButton}
                color="inherit"
                aria-label="menu"
              >
                <MenuIcon />
              </IconButton>
            )}
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={() => setAnchorEl(null)}
            >
              <MenuItem onClick={handleClose("/nuevo-heroe")}>
                Nuevo Heroe
              </MenuItem>
              <MenuItem onClick={handleClose("/registrar-mision")}>
                Registrar Mision
              </MenuItem>
              <MenuItem onClick={handleClose("/nuevo-poder")}>
                Nuevo Poder
              </MenuItem>
              <MenuItem onClick={handleClose("/registrar-planeta")}>
                Registrar Planeta
              </MenuItem>
            </Menu>
          </Grid>
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
}
