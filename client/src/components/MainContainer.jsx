import React from "react";
import Box from "@material-ui/core/Box";

export default function MainContainer({ children }) {
  return (
    <Box
      marginBottom="4rem"
      marginTop="4rem"
      maxWidth="100%"
      marginLeft="1.5rem"
      marginRight="1.5rem"
    >
      {children}
    </Box>
  );
}
