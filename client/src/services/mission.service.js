import axios from "./axios.config";

class MissionService {
  async createMission(data) {
    let res = await axios.post(`/api/mission`, data);
    return res;
  }
  async getRecentMissions() {
    let res = await axios.get(`/api/mission`);
    return res;
  }
  async getPaginatedMissions(pageNumber) {
    let res = await axios.get(`/api/mission/page/${pageNumber}`);
    return res;
  }
  async getMissionDetails(missionId) {
    let res = await axios.get(`/api/mission/${missionId}`);
    return res;
  }
}

export default new MissionService();
