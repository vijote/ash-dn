import axios from "./axios.config";

class SuperHeroService {
  async createSuperHero(data) {
    let res = await axios.post(`/api/superhero`, data);
    return res;
  }
  async getAllHeroes() {
    let res = await axios.get(`/api/superhero`);
    return res;
  }
  async getHeroDetails(heroId) {
    let res = await axios.get(`/api/superhero/${heroId}`);
    return res;
  }
  async confirmDeath(heroId) {
    let res = await axios.get(`/api/superhero/death/${heroId}`);
    return res;
  }
}

export default new SuperHeroService();
