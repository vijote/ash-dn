import axios from "./axios.config";

class PlanetService {
  async createPlanet(data) {
    let res = await axios.post(`/api/planet`, data);
    return res;
  }
  async getAllPlanets() {
    let res = await axios.get(`/api/planet`);
    return res;
  }
}

export default new PlanetService();
