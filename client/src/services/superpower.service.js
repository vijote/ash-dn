import axios from "./axios.config";

class SuperPowerService {
  async createSuperPower(data) {
    let res = await axios.post(`/api/superpower`, data);
    return res;
  }
  async getAllPowers() {
    let res = await axios.get(`/api/superpower`);
    return res;
  }
}

export default new SuperPowerService();
