import axios from "axios";

export default axios.create({
  baseURL:
    process.env.NODE_ENV === "development"
      ? `${process.env.REACT_APP_HOST}:${process.env.REACT_APP_PORT}`
      : `${process.env.REACT_APP_HOST}`,
  headers: {
    "Content-type": "application/json",
  },
});
