const reducer = (state, action) => {
  /**
     action example: {
         type: 'edit' || 'reset',
         propName: String,
         newText: String
     }
     */
  const { propName, newText } = action;
  let newState = { ...state };
  switch (action.type) {
    case "edit":
      newState[propName] = newText;
      return newState;
    case "reset":
      newState[propName] = "";
      return newState;
    default:
      throw new Error("Invalid action type");
  }
};

export default reducer;
