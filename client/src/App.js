import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import NavBar from "./components/NavBar";
import Footer from "./components/Footer";

import Home from "./pages/Home";
import Mision from "./pages/MissionDetails";
import NewPlanet from "./pages/NewPlanet";

import ThemeProvider from "@material-ui/styles/ThemeProvider";
import theme from "./theme";

import Background from "./components/Background";
import NewPower from "./pages/NewPower";
import NewSuperHero from "./pages/NewSuperHero/NewSuperHero";
import Page404 from "./pages/Page404";
import NewMission from "./pages/NewMission/NewMission";
import Missions from "./pages/Missions";
import SuperHeroDetails from "./pages/SuperHeroDetails";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Background>
          <NavBar />
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/mision/:id" exact>
              <Mision />
            </Route>
            <Route path="/registrar-planeta" exact>
              <NewPlanet />
            </Route>
            <Route path="/nuevo-poder" exact>
              <NewPower />
            </Route>
            <Route path="/nuevo-heroe" exact>
              <NewSuperHero />
            </Route>
            <Route path="/registrar-mision" exact>
              <NewMission />
            </Route>
            <Route path="/misiones" exact>
              <Missions />
            </Route>
            <Route path="/superheroe/:id" exact>
              <SuperHeroDetails />
            </Route>
            <Route path="*">
              <Page404 />
            </Route>
          </Switch>
          <Footer />
        </Background>
      </Router>
    </ThemeProvider>
  );
}

export default App;
