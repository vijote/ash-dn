import { createMuiTheme } from "@material-ui/core/styles";

export default createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#FF0429",
    },
    secondary: {
      main: "#F4FF81",
    },
    background: {
      main: "#1D314B",
    },
    footerBackground: {
      main: "#1A2738",
    },
    navbarBackground: {
      main: "#2E5584",
    },
    deathButton: {
      main: "#FF8A80",
    },
    text: {
      primary: "#fefefe",
    },
  },
});
