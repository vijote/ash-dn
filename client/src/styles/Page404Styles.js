import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  message: {
    position: "fixed",
    zIndex: 1,
    top: "6rem",
    left: "2rem",
    right: 0,
    maxWidth: "100vw",
  },
  heroImg: {
    [theme.breakpoints.down("sm")]: {
      position: "fixed",
      right: "1rem",
      bottom: "7.75rem",
      //   maxWidth: "90vw",
      height: "55vh",
      marginLeft: 0,
      zIndex: 0,
    },
    // marginLeft: "1rem",
  },
}));

export default useStyles;
