import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  grid: {
    justifyContent: "flex-end",
    [theme.breakpoints.up("sm")]: {
      justifyContent: "center",
    },
  },
  input: {
    width: "90%",
    [theme.breakpoints.up("sm")]: {
      width: "70%",
    },
    // marginLeft: "1rem",
  },
  confirmButton: {
    // marginLeft: "2rem",
    width: "100%",
    paddingTop: "0.5rem",
    paddingBottom: "0.5rem",
    marginTop: "1rem",
    [theme.breakpoints.up("sm")]: {
      //   marginBottom: "2rem",
      marginTop: 0,
      width: "80%",
    },
    // padding: 0,
    maxHeight: 56,
  },
}));

export default useStyles;
