import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  registerContainer: {
    [theme.breakpoints.down("xs")]: {
      marginTop: "1rem",
      marginLeft: 0,
    },
    marginLeft: "1rem",
  },
}));

export default useStyles;
