import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  input: {
    width: "100%",
    marginBottom: "2rem",
  },
  snackbar: {
    backgroundColor: theme.palette.secondary,
    color: "#242424",
  },
}));

export default useStyles;
