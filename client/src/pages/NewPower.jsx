import React from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import MainContainer from "../components/MainContainer";
import Styles from "../styles/NewPlanetStyles";
import reducer from "../reducer/form.reducer";
import SuperPowerService from "../services/superpower.service";
import { useHistory } from "react-router";

export default function NewPower() {
  const history = useHistory();
  const classes = Styles();
  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
    });

  const [sendingData, setSendingData] = React.useState(false);
  const [dataSent, setDataSent] = React.useState(false);
  const [errorSending, setErrorSending] = React.useState(false);
  const [state, dispatch] = React.useReducer(reducer, {
    name: "",
    description: "",
  });

  const handleChange = (propName) => (e) => {
    dispatch({ type: "edit", propName, newText: e.target.value });
  };

  const handleSubmit = async () => {
    try {
      setSendingData(true);
      await SuperPowerService.createSuperPower(state);
      dispatch({ type: "reset" });
      setSendingData(false);
      setDataSent(true);
      history.push("/");
    } catch (error) {
      setSendingData(false);
      setErrorSending(true);
    }
  };

  return (
    <MainContainer>
      <Box marginBottom="3rem">
        <Typography variant="h4" color="textPrimary" component="h1">
          Cargar Super Poder
        </Typography>
      </Box>
      <ThemeProvider theme={textFieldTheme}>
        <TextField
          className={classes.input}
          id="outlined-basic"
          value={state.name}
          onChange={handleChange("name")}
          label="Nombre del Poder"
          variant="outlined"
          color="primary"
        />
      </ThemeProvider>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Registrar
      </Button>
      <Snackbar
        // autoHideDuration={3000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={sendingData}
        onClose={() => setSendingData(!sendingData)}
        message="Enviando..."
        key={"verticalbottom"}
      />
      <Snackbar
        open={dataSent}
        autoHideDuration={6000}
        onClose={() => setDataSent(!dataSent)}
      >
        <Alert elevation={6} variant="filled" severity="success">
          Enviado!
        </Alert>
      </Snackbar>
      <Snackbar
        open={errorSending}
        autoHideDuration={6000}
        onClose={() => setErrorSending(!errorSending)}
      >
        <Alert elevation={6} variant="filled" severity="error">
          {`Ocurrio un error :(`}
        </Alert>
      </Snackbar>
    </MainContainer>
  );
}
