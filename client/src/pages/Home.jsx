import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Styles from "../styles/HomePageStyles";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import MainContainer from "../components/MainContainer";
import RedirectButton from "../components/RedirectButton";
import { useHistory } from "react-router";
import missionService from "../services/mission.service";

export default function Home() {
  const [recentMissions, setRecentMissions] = React.useState([]);
  const history = useHistory();
  const classes = Styles();

  const getData = async () => {
    const res = await missionService.getRecentMissions();
    console.log(res);
    setRecentMissions(res.data.missions);
  };

  React.useEffect(() => {
    if (!recentMissions.length) {
      getData();
    }
  }, [recentMissions]);
  return (
    <MainContainer>
      {recentMissions.length ? (
        <>
          <Typography variant="h4" color="textPrimary" component="h1">
            Bienvenido
          </Typography>
          <Box marginTop="1.5rem">
            <Typography color="textPrimary">
              Aqui podra llevar control de toda la actividad realizada por los
              superheroes
            </Typography>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Box marginTop="2rem">
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => history.push("/nuevo-heroe")}
                  >
                    Nuevo Super heroe
                  </Button>
                </Box>
              </Grid>
              <Grid item xs={12} sm>
                <Box marginTop="2rem" className={classes.registerContainer}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => history.push("/registrar-mision")}
                  >
                    Registrar Mision
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
          <Box marginTop="4rem">
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography color="textPrimary">Ultimas misiones</Typography>
              <Button
                variant="text"
                color="default"
                onClick={() => history.push("/misiones")}
              >
                Ver todas
              </Button>
            </Box>
            {recentMissions.map((mission) => (
              <Box key={mission.id} marginTop="0.5rem" marginBottom="1rem">
                <RedirectButton
                  title={mission.name}
                  to={`/mision/${mission.id}`}
                />
              </Box>
            ))}
          </Box>
        </>
      ) : (
        <CircularProgress color="secondary" size={100} />
      )}
    </MainContainer>
  );
}
