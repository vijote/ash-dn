import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const todayDate = new Date().toISOString().slice(0, 10);

export default function StepOne({ onSubmit, planets }) {
  const [heroData, setHeroData] = React.useState({
    name: "",
    appearence_date: todayDate,
    planet_id: 3,
    show_planet_select: "no",
  });

  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
      overrides: {
        MuiFormControlLabel: {
          root: {
            color: "#FEFEFE",
          },
        },
        MuiPaper: {
          root: {
            backgroundColor: theme.palette.background.main,
          },
        },
      },
    });

  const handleChange = (e) => {
    setHeroData({
      ...heroData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = () => {
    if (heroData.name.length && heroData.appearence_date.length)
      onSubmit(heroData);
  };

  return (
    <>
      <Box marginBottom="3rem">
        <Typography variant="h4" color="textPrimary" component="h1">
          Nuevo Super Heroe
        </Typography>
      </Box>
      <ThemeProvider theme={textFieldTheme}>
        <Box width="100%" marginBottom="1rem">
          <TextField
            id="hero-name"
            name="name"
            value={heroData.name}
            onChange={handleChange}
            fullWidth
            error={!heroData.name.length}
            helperText={
              !heroData.name.length ? "Por favor ingrese un nombre" : ""
            }
            label="Nombre del heroe"
            variant="outlined"
            color="primary"
          />
        </Box>
        <Box width="100%" marginBottom="2rem">
          <TextField
            id="date"
            label="Fecha de Aparicion"
            name="appearence_date"
            type="date"
            variant="outlined"
            value={heroData.appearence_date}
            error={!heroData.appearence_date.length}
            helperText={
              !heroData.appearence_date.length
                ? "Por favor ingrese una fecha"
                : ""
            }
            onChange={handleChange}
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Box>
        <Typography color="primary">¿Nació en otro planeta?</Typography>
        <Box marginBottom="1rem">
          <FormControl component="fieldset">
            <RadioGroup
              aria-label="gender"
              name="show_planet_select"
              value={heroData.show_planet_select}
              onChange={handleChange}
            >
              <FormControlLabel value="si" control={<Radio />} label="Si" />
              <FormControlLabel value="no" control={<Radio />} label="No" />
            </RadioGroup>
          </FormControl>
        </Box>
        {heroData.show_planet_select === "si" && (
          <Box marginTop="1rem" marginBottom="2rem">
            <FormControl variant="outlined" fullWidth>
              <InputLabel id="demo-simple-select-outlined-label">
                Age
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                name="planet_id"
                id="demo-simple-select-outlined"
                value={heroData.planet_id}
                onChange={handleChange}
                label="Age"
              >
                {planets.map((planet) => (
                  <MenuItem key={planet.id} value={planet.id}>
                    {planet.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
        )}
      </ThemeProvider>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Continuar
      </Button>
    </>
  );
}
