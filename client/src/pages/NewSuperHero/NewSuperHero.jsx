import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import MainContainer from "../../components/MainContainer";
import planetService from "../../services/planet.service";
import superPowerService from "../../services/superpower.service";
import superHeroService from "../../services/superhero.service";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import StepThree from "./StepThree";
import { useHistory } from "react-router";

export default function NewSuperHero() {
  const history = useHistory();
  const [heroData, setHeroData] = React.useState({});
  const [currentStep, setCurrentStep] = React.useState(1);
  const [allPlanets, setAllPlanets] = React.useState([]);
  const [allPowers, setAllPowers] = React.useState([]);
  const [allHeroes, setAllHeroes] = React.useState([]);

  const getData = async () => {
    try {
      let promises = [];
      promises.push(planetService.getAllPlanets());
      promises.push(superPowerService.getAllPowers());
      promises.push(superHeroService.getAllHeroes());
      const [planets, superpowers, superheroes] = await Promise.all(promises);
      console.log(superheroes);
      setAllPlanets(planets.data.planets);
      setAllPowers(superpowers.data.super_powers);
      setAllHeroes(superheroes.data.super_heroes);
    } catch (error) {
      console.log(error);
    }
  };

  const onSubmit = (data) => {
    setCurrentStep(currentStep + 1);
    setHeroData({ ...heroData, ...data });
  };

  const finalSubmit = async (data) => {
    await superHeroService.createSuperHero({ ...heroData, ...data });
    history.push("/");
  };

  React.useEffect(() => {
    if (!allPlanets.length && !allPowers.length && !allHeroes.length) {
      getData();
    }
  }, [allPlanets, allPowers, allHeroes]);

  let step = <></>;
  switch (currentStep) {
    case 1:
      step = <StepOne planets={allPlanets} onSubmit={onSubmit} />;
      break;
    case 2:
      step = <StepTwo powers={allPowers} onSubmit={onSubmit} />;
      break;
    case 3:
      step = <StepThree heroes={allHeroes} onSubmit={finalSubmit} />;
      break;
    default:
      throw new Error("Invalid Step!");
  }
  return (
    <MainContainer>
      {allPowers.length && allPlanets.length ? (
        step
      ) : (
        <CircularProgress color="secondary" size={100} />
      )}
    </MainContainer>
  );
}
