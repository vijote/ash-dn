import React from "react";
import Box from "@material-ui/core/Box";
import Radio from "@material-ui/core/Radio";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import InputLabel from "@material-ui/core/InputLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import FormHelperText from "@material-ui/core/FormHelperText";
import MainContainer from "../../components/MainContainer";
import Grid from "@material-ui/core/Grid";
import Check from "@material-ui/icons/Check";
import Delete from "@material-ui/icons/Delete";
import Styles from "../../styles/newSuperHeroStyles";
import { v4 as uuid } from "uuid";

export default function StepTwo({ onSubmit, powers }) {
  const classes = Styles();
  const [heroPowers, setHeroPowers] = React.useState({
    super_poder_id: 1,
    super_poder_level: "1",
  });
  const [heroData, setHeroData] = React.useState({
    super_powers: [],

    inmortal: "no",
  });

  const addNewPower = () => {
    if (
      heroPowers.super_poder_id &&
      heroPowers.super_poder_id !== 1 &&
      parseFloat(heroPowers.super_poder_level) >= 1
    ) {
      setHeroData({
        ...heroData,
        super_powers: [
          ...heroData.super_powers,
          {
            id: uuid(),
            super_poder_id: heroPowers.super_poder_id,
            super_poder_level: heroPowers.super_poder_level,
          },
        ],
      });
      setHeroPowers({
        super_poder_id: 1,
        super_poder_level: "1",
      });
    }
  };

  const handleChange = (e) => {
    setHeroData({
      ...heroData,
      [e.target.name]: e.target.value,
    });
  };

  const handleAddChange = (e) => {
    setHeroPowers({
      ...heroPowers,
      [e.target.name]: e.target.value,
    });
  };

  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
      overrides: {
        MuiFormControlLabel: {
          root: {
            color: "#FEFEFE",
          },
        },
        MuiPaper: {
          root: {
            backgroundColor: theme.palette.background.main,
          },
        },
      },
    });

  const handleSubmit = () => {
    if (heroData.super_powers.length && heroData.inmortal) {
      onSubmit(heroData);
    }
    console.log(heroData);
  };

  const handleDelete = (power_id) => () => {
    const newPowers = [...heroData.super_powers];
    setHeroData({
      ...heroData,
      super_powers: newPowers.filter((power) => power.id !== power_id),
    });
  };

  return (
    <MainContainer>
      <Box marginBottom="3rem">
        <Typography variant="h4" color="textPrimary" component="h1">
          Nuevo Super Heroe
        </Typography>
      </Box>
      <ThemeProvider theme={textFieldTheme}>
        <Box marginBottom="1rem">
          <Typography color="primary">Super Poderes</Typography>
        </Box>
        <Grid container>
          <Grid item xs={6}>
            <FormControl variant="outlined" fullWidth>
              <InputLabel
                // error={!heroData.super_powers.length}
                id="super_power"
              >
                Super Poder
              </InputLabel>
              <Select
                labelId="super_power"
                label="Super Poder"
                name="super_poder_id"
                id="super_power_id"
                value={heroPowers.super_poder_id}
                // error={!heroData.super_powers.length}
                onChange={handleAddChange}
              >
                <MenuItem value={1}>Seleccione</MenuItem>
                {powers.map((superpower) => (
                  <MenuItem key={superpower.id} value={superpower.id}>
                    {superpower.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item container xs={6} sm={4} className={classes.grid}>
            <TextField
              id="hero-level"
              name="super_poder_level"
              type="number"
              inputProps={{ min: 1 }}
              className={classes.input}
              value={heroPowers.super_poder_level}
              onChange={handleAddChange}
              error={!(parseFloat(heroPowers.super_poder_level) > 0)}
              helperText={
                !(parseFloat(heroPowers.super_poder_level) > 0)
                  ? "ingrese nivel"
                  : ""
              }
              label="Nivel"
              variant="outlined"
              color="primary"
            />
          </Grid>
          <Grid item container xs={12} sm={2} justify="flex-end">
            <Button
              variant="outlined"
              color="secondary"
              className={classes.confirmButton}
              onClick={addNewPower}
            >
              <Check />
            </Button>
          </Grid>
        </Grid>
        {heroData.super_powers.map((power) => (
          <Box
            key={power.id}
            paddingTop="1rem"
            paddingBottom="1rem"
            paddingLeft="1rem"
            paddingRight="1rem"
            border="1px solid #F4FF81"
            borderRadius="0.5rem"
            marginTop="1rem"
            display="flex"
            justifyContent="space-between"
          >
            <Typography color="primary">
              {powers.find((p) => p.id === power.super_poder_id).name}
            </Typography>
            <Delete color="error" onClick={handleDelete(power.id)} />
          </Box>
        ))}
        {!heroData.super_powers.length && (
          <FormHelperText error={true}>Cargue al menos un poder</FormHelperText>
        )}
        <Box marginTop="3rem">
          <Typography color="primary">¿Es inmortal?</Typography>
          <Box marginBottom="3rem">
            <FormControl component="fieldset">
              <RadioGroup
                aria-label="gender"
                name="inmortal"
                value={heroData.inmortal}
                onChange={handleChange}
              >
                <FormControlLabel value="si" control={<Radio />} label="Si" />
                <FormControlLabel value="no" control={<Radio />} label="No" />
              </RadioGroup>
            </FormControl>
          </Box>
        </Box>
      </ThemeProvider>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Continuar
      </Button>
    </MainContainer>
  );
}
