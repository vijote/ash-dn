import React from "react";
import Box from "@material-ui/core/Box";
import Radio from "@material-ui/core/Radio";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import InputLabel from "@material-ui/core/InputLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import Snackbar from "@material-ui/core/Snackbar";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import FormHelperText from "@material-ui/core/FormHelperText";
import MainContainer from "../../components/MainContainer";
import Grid from "@material-ui/core/Grid";
import Check from "@material-ui/icons/Check";
import Delete from "@material-ui/icons/Delete";
import { v4 as uuid } from "uuid";

export default function StepThree({ onSubmit, heroes }) {
  const [sendingData, setSendingData] = React.useState(false);
  const [heroId, setHeroId] = React.useState(1);
  const [heroData, setHeroData] = React.useState({
    companions: [],
    has_companion: "no",
  });

  const addCompanion = () => {
    if (heroId && heroId !== 1) {
      setHeroData({
        ...heroData,
        companions: [
          ...heroData.companions,
          {
            id: uuid(),
            super_hero_id: heroId,
          },
        ],
      });
      setHeroId(1);
    }
  };

  const handleHeroIdChange = (e) => {
    setHeroId(e.target.value);
  };

  const handleChange = (e) => {
    setHeroData({
      ...heroData,
      [e.target.name]: e.target.value,
    });
  };

  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
      overrides: {
        MuiFormControlLabel: {
          root: {
            color: "#FEFEFE",
          },
        },
        MuiPaper: {
          root: {
            backgroundColor: theme.palette.background.main,
          },
        },
      },
    });

  const handleSubmit = () => {
    setSendingData(true);
    if (heroData.has_companion && heroData.has_companion === "si") {
      if (heroData.companions.length) {
        onSubmit(heroData);
      }
    } else if (heroData.has_companion && heroData.has_companion === "no") {
      onSubmit(heroData);
    }
  };

  const handleDelete = (hero_id) => () => {
    setHeroData({
      ...heroData,
      companions: heroData.companions.filter(
        (heroe) => heroe.super_hero_id !== hero_id
      ),
    });
  };

  return (
    <MainContainer>
      <Box marginBottom="3rem">
        <Typography variant="h4" color="textPrimary" component="h1">
          Nuevo Super Heroe
        </Typography>
      </Box>
      <ThemeProvider theme={textFieldTheme}>
        <Box marginTop="3rem">
          <Typography color="primary">¿Tiene accompañantes?</Typography>
          <Box marginBottom="2rem">
            <FormControl component="fieldset">
              <RadioGroup
                aria-label="gender"
                name="has_companion"
                value={heroData.has_companion}
                onChange={handleChange}
              >
                <FormControlLabel value="si" control={<Radio />} label="Si" />
                <FormControlLabel value="no" control={<Radio />} label="No" />
              </RadioGroup>
            </FormControl>
          </Box>
        </Box>
        {heroData.has_companion === "si" && (
          <>
            <Box marginBottom="1rem">
              <Typography color="primary">Acompañantes</Typography>
            </Box>
            <Grid container justify="space-between">
              <Grid item xs={9}>
                <FormControl variant="outlined" fullWidth>
                  <InputLabel
                    // error={!heroData.super_powers.length}
                    id="super_power"
                  >
                    Acompañante
                  </InputLabel>
                  <Select
                    labelId="super_power"
                    label="Acompañante"
                    name="super_poder_id"
                    id="super_power_id"
                    value={heroId}
                    // error={!heroData.super_powers.length}
                    onChange={handleHeroIdChange}
                  >
                    <MenuItem value={1}>Seleccione</MenuItem>
                    {heroes.map((heroe) => (
                      <MenuItem key={heroe.id} value={heroe.id}>
                        {heroe.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item container xs={2} justify="flex-end">
                <Button
                  variant="outlined"
                  color="secondary"
                  //   className={classes.confirmButton}
                  onClick={addCompanion}
                >
                  <Check />
                </Button>
              </Grid>
            </Grid>
            {heroData.companions.map((heroe) => (
              <Box
                key={heroe.id}
                paddingTop="1rem"
                paddingBottom="1rem"
                paddingLeft="1rem"
                paddingRight="1rem"
                border="1px solid #F4FF81"
                borderRadius="0.5rem"
                marginTop="1rem"
                display="flex"
                justifyContent="space-between"
              >
                <Typography color="primary">
                  {heroes.find((h) => h.id === heroe.super_hero_id).name}
                </Typography>
                <Delete
                  color="error"
                  onClick={handleDelete(heroe.super_hero_id)}
                />
              </Box>
            ))}
            {!heroData.companions.length && (
              <FormHelperText error={true}>
                Cargue al menos un compañero
              </FormHelperText>
            )}
          </>
        )}
      </ThemeProvider>
      <Box marginTop="2rem" />
      <Button
        fullWidth
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Continuar
      </Button>
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={sendingData}
        onClose={() => setSendingData(false)}
        message="Enviando..."
        key="bottom-center"
      />
    </MainContainer>
  );
}
