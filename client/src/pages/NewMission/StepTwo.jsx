import React from "react";
import Box from "@material-ui/core/Box";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import RadioGroup from "@material-ui/core/RadioGroup";
import Snackbar from "@material-ui/core/Snackbar";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

export default function StepTwo({ onSubmit }) {
  const [sendingData, setSendingData] = React.useState(false);
  const [heroData, setHeroData] = React.useState({
    type: "mr",
    description: "",
  });

  const handleChange = (e) => {
    setHeroData({
      ...heroData,
      [e.target.name]: e.target.value,
    });
  };

  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
      overrides: {
        MuiFormControlLabel: {
          root: {
            color: "#FEFEFE",
          },
        },
        MuiPaper: {
          root: {
            backgroundColor: theme.palette.background.main,
          },
        },
      },
    });

  const handleSubmit = () => {
    setSendingData(true);
    if (heroData.description.length) {
      onSubmit(heroData);
    }
  };

  return (
    <>
      <ThemeProvider theme={textFieldTheme}>
        <Box marginBottom="3rem">
          <Typography variant="h4" color="textPrimary" component="h1">
            Registrar Mision
          </Typography>
        </Box>
        <Box marginBottom="2rem">
          <FormControl component="fieldset">
            <RadioGroup
              aria-label="type of mission"
              name="type"
              value={heroData.type}
              onChange={handleChange}
            >
              <FormControlLabel
                value="mr"
                control={<Radio />}
                label="Mision Rutinaria"
              />
              <FormControlLabel
                value="msu"
                control={<Radio />}
                label="Mision Salvar el Universo"
              />
            </RadioGroup>
          </FormControl>
        </Box>
        <Box width="100%" marginBottom="3rem">
          <TextField
            id="mission-description"
            name="description"
            value={heroData.description}
            onChange={handleChange}
            fullWidth
            error={!heroData.description.length}
            helperText={
              !heroData.description.length ? "Por favor ingrese un nombre" : ""
            }
            label="Descripcion"
            multiline
            // rows={6}
            variant="outlined"
            color="primary"
          />
        </Box>
      </ThemeProvider>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Continuar
      </Button>
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={sendingData}
        onClose={() => setSendingData(false)}
        message="Enviando..."
        key="bottom-center"
      />
    </>
  );
}
