import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import Check from "@material-ui/icons/Check";
import Delete from "@material-ui/icons/Delete";
import { v4 as uuid } from "uuid";

const todayDate = new Date().toISOString().slice(0, 10);

export default function StepOne({ heroes, onSubmit }) {
  const [heroId, setHeroId] = React.useState(1);
  const [heroData, setHeroData] = React.useState({
    heroes: [],
    realization_date: todayDate,
    name: "",
  });

  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
      overrides: {
        MuiFormControlLabel: {
          root: {
            color: "#FEFEFE",
          },
        },
        MuiPaper: {
          root: {
            backgroundColor: theme.palette.background.main,
          },
        },
      },
    });

  const handleHeroIdChange = (e) => {
    setHeroId(e.target.value);
  };

  const handleChange = (e) => {
    setHeroData({
      ...heroData,
      [e.target.name]: e.target.value,
    });
  };

  const addMember = () => {
    if (heroId && heroId !== 1) {
      setHeroData({
        ...heroData,
        heroes: [
          ...heroData.heroes,
          {
            id: uuid(),
            super_hero_id: heroId,
          },
        ],
      });
      setHeroId(1);
    }
  };

  const handleDelete = (hero_id) => () => {
    setHeroData({
      ...heroData,
      heroes: heroData.heroes.filter(
        (heroe) => heroe.super_hero_id !== hero_id
      ),
    });
  };

  const handleSubmit = () => {
    onSubmit(heroData);
  };

  return (
    <>
      <Box marginBottom="3rem">
        <Typography variant="h4" color="textPrimary" component="h1">
          Registrar Mision
        </Typography>
      </Box>
      <ThemeProvider theme={textFieldTheme}>
        <Box width="100%" marginBottom="1rem">
          <TextField
            id="mission-name"
            name="name"
            value={heroData.name}
            onChange={handleChange}
            fullWidth
            error={!heroData.name.length}
            helperText={!heroData.name.length ? "Ingrese un nombre" : ""}
            label="Nombre de la mision"
            variant="outlined"
            color="primary"
          />
        </Box>
        <Box width="100%" marginBottom="2rem">
          <TextField
            id="realization date"
            label="Fecha de Realizacion"
            name="realization_date"
            type="date"
            variant="outlined"
            value={heroData.realization_date}
            error={!heroData.realization_date.length}
            helperText={
              !heroData.realization_date.length
                ? "Por favor ingrese una fecha"
                : ""
            }
            onChange={handleChange}
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Box>
        <Box marginBottom="1rem">
          <Typography color="primary">Heroes de la mision</Typography>
        </Box>
        <Grid container justify="space-between">
          <Grid item xs={8}>
            <FormControl variant="outlined" fullWidth>
              <InputLabel error={!heroData.heroes.length} id="super_power">
                Super Heroe
              </InputLabel>
              <Select
                labelId="super_power"
                label="Super Heroe"
                name="super_poder_id"
                id="super_power_id"
                value={heroId}
                error={!heroData.heroes.length}
                onChange={handleHeroIdChange}
              >
                <MenuItem value={1}>Seleccione</MenuItem>
                {heroes.map((heroe) => (
                  <MenuItem key={heroe.id} value={heroe.id}>
                    {heroe.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item container xs={2} justify="flex-end">
            <Button variant="outlined" color="secondary" onClick={addMember}>
              <Check />
            </Button>
          </Grid>
        </Grid>
        {heroData.heroes.map((heroe) => (
          <Box
            key={heroe.id}
            paddingTop="1rem"
            paddingBottom="1rem"
            paddingLeft="1rem"
            paddingRight="1rem"
            border="1px solid #F4FF81"
            borderRadius="0.5rem"
            marginTop="1rem"
            display="flex"
            justifyContent="space-between"
          >
            <Typography color="primary">
              {heroes.find((h) => h.id === heroe.super_hero_id).name}
            </Typography>
            <Delete color="error" onClick={handleDelete(heroe.super_hero_id)} />
          </Box>
        ))}
        {!heroData.heroes.length && (
          <FormHelperText error={true}>
            Cargue al menos un participante
          </FormHelperText>
        )}
      </ThemeProvider>
      <Box marginTop="3rem" />
      <Button
        fullWidth
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Continuar
      </Button>
    </>
  );
}
