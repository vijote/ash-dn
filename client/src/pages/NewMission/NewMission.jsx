import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import MainContainer from "../../components/MainContainer";
import superHeroService from "../../services/superhero.service";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import missionService from "../../services/mission.service";
import { useHistory } from "react-router";

export default function NewMission() {
  const history = useHistory();
  const [currentStep, setCurrentStep] = React.useState(1);
  const [heroData, setHeroData] = React.useState({});
  const [allHeroes, setAllHeroes] = React.useState([]);

  const getData = async () => {
    const res = await superHeroService.getAllHeroes();
    setAllHeroes(res.data.super_heroes);
  };

  let step = <></>;

  const onSubmit = (data) => {
    setHeroData({ ...heroData, ...data });
    setCurrentStep(currentStep + 1);
  };

  const finalSubmit = async (data) => {
    console.log({ ...heroData, ...data });
    await missionService.createMission({ ...heroData, ...data });
    history.push("/");
  };

  React.useEffect(() => {
    if (!allHeroes.length) {
      getData();
    }
  }, [allHeroes]);

  switch (currentStep) {
    case 1:
      step = <StepOne heroes={allHeroes} onSubmit={onSubmit} />;
      break;
    case 2:
      step = <StepTwo onSubmit={finalSubmit} />;
      break;
    default:
      throw new Error("Invalid Step!");
  }

  return (
    <MainContainer>
      {allHeroes.length ? (
        step
      ) : (
        <CircularProgress color="secondary" size={100} />
      )}
    </MainContainer>
  );
}
