import React from "react";
import MainContainer from "../components/MainContainer";
import HeroImg from "../assets/iron-man.png";
import Styles from "../styles/Page404Styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router";

export default function Page404() {
  const history = useHistory();
  const classes = Styles();
  return (
    <MainContainer>
      <Box width="100%" className={classes.message}>
        <Box marginBottom="1rem">
          <Typography color="textPrimary">
            Oops! Esta pagina no existe
          </Typography>
        </Box>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push("/")}
        >
          Ir al inicio
        </Button>
      </Box>
      <img
        src={HeroImg}
        className={classes.heroImg}
        alt="Page Not found"
        style={{ maxHeight: "90vh", marginBottom: "-4.25rem" }}
      />
    </MainContainer>
  );
}
