import React from "react";
import Typography from "@material-ui/core/Typography";
import MainContainer from "../components/MainContainer";
import Box from "@material-ui/core/Box";
import RedirectButton from "../components/RedirectButton";
import CircularProgress from "@material-ui/core/CircularProgress";
import missionService from "../services/mission.service";
import { useParams } from "react-router";

export default function MissionDetails() {
  const params = useParams();
  const [missionDetails, setMissionDetails] = React.useState({});

  const getData = async () => {
    const res = await missionService.getMissionDetails(params.id);
    console.log(res.data.missionDetails);
    setMissionDetails(res.data.missionDetails);
  };

  React.useEffect(() => {
    if (!missionDetails.name) {
      getData();
    }
  });

  return (
    <MainContainer>
      {missionDetails.name ? (
        <>
          <Typography variant="h4" color="textPrimary" component="h1">
            {missionDetails.name}
          </Typography>
          {missionDetails.type ? (
            <Box marginTop="0.5rem">
              <Typography color="secondary">Mision Rutinaria</Typography>
            </Box>
          ) : (
            <Box marginTop="0.5rem">
              <Typography color="secondary">
                Mision Salvar el Universo
              </Typography>
            </Box>
          )}
          <Box marginTop="2.5rem">
            <Typography color="textPrimary">
              {missionDetails.description}
            </Typography>
          </Box>
          <Box marginTop="2rem">
            <Typography color="textPrimary">Heroes participantes:</Typography>
          </Box>
          <Box marginTop="2rem">
            {missionDetails.super_heros.map((hero) => (
              <Box key={hero.id} marginTop="1rem">
                <RedirectButton
                  title={hero.name}
                  to={`/superheroe/${hero.id}`}
                />
              </Box>
            ))}
          </Box>
        </>
      ) : (
        <CircularProgress color="secondary" size={100} />
      )}
    </MainContainer>
  );
}
