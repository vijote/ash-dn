import React from "react";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import MainContainer from "../components/MainContainer";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import Styles from "../styles/NewPlanetStyles";
import planetService from "../services/planet.service";
import { useHistory } from "react-router";

export default function NewPlanet() {
  const history = useHistory();
  const [planetName, setPlanetName] = React.useState("");
  const [dataSent, setDataSent] = React.useState(false);
  const [sendingData, setSendingData] = React.useState(false);
  const [errorSending, setErrorSending] = React.useState(false);
  const classes = Styles();

  const handleSubmit = async () => {
    if (planetName.length) {
      try {
        setSendingData(true);
        await planetService.createPlanet({ name: planetName });
        setSendingData(false);
        setDataSent(true);
        history.push("/");
      } catch (error) {
        setSendingData(false);
        setErrorSending(true);
      }
    }
  };

  const handleChange = async (e) => {
    setPlanetName(e.target.value);
  };

  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
    });
  return (
    <MainContainer>
      <Box marginBottom="3rem">
        <Typography variant="h4" color="textPrimary" component="h1">
          Registrar Planeta
        </Typography>
      </Box>
      <ThemeProvider theme={textFieldTheme}>
        <TextField
          className={classes.input}
          id="outlined-basic"
          value={planetName}
          onChange={handleChange}
          label="Nombre del planeta"
          variant="outlined"
          color="primary"
        />
      </ThemeProvider>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        onClick={handleSubmit}
      >
        Registrar
      </Button>
      <Snackbar
        className={classes.snackbar}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={sendingData}
        onClose={() => setSendingData(!sendingData)}
        message="Enviando..."
        key={"verticalbottom"}
      />
      <Snackbar
        open={dataSent}
        autoHideDuration={6000}
        onClose={() => setDataSent(!dataSent)}
      >
        <Alert elevation={6} variant="filled" severity="success">
          Enviado!
        </Alert>
      </Snackbar>
      <Snackbar
        open={errorSending}
        autoHideDuration={6000}
        onClose={() => setErrorSending(!errorSending)}
      >
        <Alert elevation={6} variant="filled" severity="error">
          {`Ocurrio un error :(`}
        </Alert>
      </Snackbar>
    </MainContainer>
  );
}
