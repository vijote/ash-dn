import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Snackbar from "@material-ui/core/Snackbar";
import CircularProgress from "@material-ui/core/CircularProgress";
import MainContainer from "../components/MainContainer";
import superheroService from "../services/superhero.service";
import { useParams } from "react-router";
import { Button } from "@material-ui/core";

export default function SuperHeroDetails() {
  const [sendingData, setSendingData] = React.useState(false);
  const params = useParams();
  const [heroDetails, setHeroDetails] = React.useState({});

  const getData = async () => {
    const res = await superheroService.getHeroDetails(params.id);
    console.log(res);
    setHeroDetails(res.data.super_hero);
  };

  const confirmDeath = async () => {
    setSendingData(true);
    await superheroService.confirmDeath(params.id);
    getData();
  };

  React.useEffect(() => {
    if (!heroDetails.name) {
      getData();
    }
  });
  return (
    <MainContainer>
      {heroDetails.name ? (
        <>
          <Typography variant="h4" color="textPrimary" component="h1">
            {heroDetails.name}
          </Typography>
          <Box marginTop="2.5rem">
            <Typography color="textPrimary">Fecha de aparicion:</Typography>
            <Box marginTop="0.5rem">
              <Typography color="secondary">
                {heroDetails.appearence_date.slice(0, 10)}
              </Typography>
            </Box>
          </Box>
          <Box marginTop="2.5rem">
            <Typography color="textPrimary">Super poderes:</Typography>
            {heroDetails.super_powers.map((power) => (
              <Box key={power.id} marginTop="0.5rem">
                <Typography color="secondary">{power.name}</Typography>
              </Box>
            ))}
          </Box>
          {heroDetails.companions.length > 0 && (
            <Box marginTop="2.5rem">
              <Typography color="textPrimary">Acompañantes:</Typography>
              {heroDetails.companions.map((hero) => (
                <Box key={hero.id} marginTop="0.5rem">
                  <Typography color="secondary">{hero.name}</Typography>
                </Box>
              ))}
            </Box>
          )}
          <Box marginTop="2.5rem">
            <Typography color="textPrimary">Planeta de Origen:</Typography>
            <Box marginTop="0.5rem">
              <Typography color="secondary">
                {heroDetails.home_planet}
              </Typography>
            </Box>
          </Box>
          {heroDetails.death_date ? (
            <Box marginTop="2.5rem">
              <Box marginTop="0.5rem">
                <Typography color="primary">Fallecido</Typography>
              </Box>
            </Box>
          ) : (
            <Box marginTop="2rem">
              <Button variant="outlined" onClick={confirmDeath} color="primary">
                Registrar Muerte
              </Button>
            </Box>
          )}
        </>
      ) : (
        <CircularProgress color="secondary" size={100} />
      )}
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={sendingData}
        onClose={() => setSendingData(!sendingData)}
        message="Enviando..."
        key={"verticalbottom"}
      />
    </MainContainer>
  );
}
