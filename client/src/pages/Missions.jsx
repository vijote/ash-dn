import React from "react";
import Typography from "@material-ui/core/Typography";
import MainContainer from "../components/MainContainer";
import Box from "@material-ui/core/Box";
import RedirectButton from "../components/RedirectButton";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import ChevronRight from "@material-ui/icons/ChevronRight";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import missionService from "../services/mission.service";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

export default function Missions() {
  const [currentResults, setCurrentResults] = React.useState([]);
  const [fetchingData, setFetchingData] = React.useState(true);
  const [currentPage, setCurrentPage] = React.useState(1);
  const [resultCount, setResultCount] = React.useState(1);

  const textFieldTheme = (theme) =>
    createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary,
        },
      },
      overrides: {
        MuiFormControlLabel: {
          root: {
            color: "#FEFEFE",
          },
        },
        MuiPaper: {
          root: {
            backgroundColor: theme.palette.background.main,
          },
        },
      },
    });

  const getData = async (page) => {
    const res = await missionService.getPaginatedMissions(page);
    console.log(res.data);
    setCurrentResults(res.data.allMissions.rows);
    setResultCount(res.data.allMissions.count);
    setFetchingData(false);
    setCurrentPage(page);
    // console.log(res);
  };

  React.useEffect(() => {
    if (!currentResults.length) {
      getData(currentPage);
    }
  }, [currentResults, currentPage]);

  const nextPage = () => {
    setFetchingData(true);
    getData(currentPage + 1);
  };

  const previousPage = () => {
    setFetchingData(true);
    getData(currentPage - 1);
  };

  return (
    <MainContainer>
      {!fetchingData ? (
        <>
          <Typography variant="h4" color="textPrimary" component="h1">
            Registro de Misiones
          </Typography>
          <Box marginTop="2.5rem">
            <Typography color="textPrimary">
              A continuacion se muestran todas las misiones registradas,
              ordenadas por fecha:
            </Typography>
          </Box>
          <Box marginTop="2rem">
            {currentResults.map((mission) => (
              <Box key={mission.id} marginTop="1rem">
                <RedirectButton
                  title={mission.name}
                  to={`/mision/${mission.id}`}
                />
              </Box>
            ))}
          </Box>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            marginTop="2rem"
          >
            <ThemeProvider theme={textFieldTheme}>
              {currentPage > 1 && (
                <Button variant="outlined">
                  <ChevronLeft color="primary" onClick={previousPage} />
                </Button>
              )}
              <Typography color="primary">Pagina {currentPage}</Typography>
              {currentPage * 4 < resultCount && (
                <Button variant="outlined">
                  <ChevronRight color="primary" onClick={nextPage} />
                </Button>
              )}
            </ThemeProvider>
          </Box>
        </>
      ) : (
        <CircularProgress color="secondary" size={100} />
      )}
    </MainContainer>
  );
}
