const fs = require("fs");
require("dotenv").config();
const NODE_ENV = process.env.NODE_ENV || "development";
if (fs.existsSync(`.env.${NODE_ENV}`)) {
  require("node-env-file")(`.env.${NODE_ENV}`);
}

const express = require("express");
const cors = require("cors");
const port = process.env.PORT || 3001;

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("API Agrupacion Super Heroes");
});

app.use("/api/planet", require("./routes/planet"));
app.use("/api/mission", require("./routes/mission"));
app.use("/api/companion", require("./routes/companion"));
app.use("/api/superhero", require("./routes/superhero"));
app.use("/api/superpower", require("./routes/superpower"));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
