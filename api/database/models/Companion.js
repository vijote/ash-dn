"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class companion extends Model {
    static associate(models) {}
  }
  companion.init(
    {
      super_hero_id: DataTypes.INTEGER,
      companion_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      createdAt: false,
      updatedAt: false,
      modelName: "companion",
    }
  );
  return companion;
};
