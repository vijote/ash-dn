"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class mission extends Model {
    static associate(models) {
      mission.belongsToMany(models.super_hero, {
        through: models.mission_participation,
        foreignKey: "mission_id",
      });
    }
  }
  mission.init(
    {
      name: DataTypes.STRING(45),
      type: DataTypes.BOOLEAN,
      description: DataTypes.STRING(300),
      completion_date: DataTypes.DATE,
    },
    {
      sequelize,
      createdAt: false,
      updatedAt: false,
      modelName: "mission",
    }
  );
  return mission;
};
