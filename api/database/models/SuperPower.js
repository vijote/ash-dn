"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class super_power extends Model {
    static associate(models) {
      // horarios atencion que pertenece a un centro
      super_power.belongsToMany(models.super_hero, {
        through: models.super_power_level,
        foreignKey: "super_power_id",
      });
    }
  }
  super_power.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      createdAt: false,
      updatedAt: false,
      modelName: "super_power",
    }
  );
  return super_power;
};
