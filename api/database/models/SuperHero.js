"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class super_hero extends Model {
    static associate(models) {
      // horarios atencion que pertenece a un centro
      super_hero.belongsToMany(models.super_power, {
        through: models.super_power_level,
        foreignKey: "super_hero_id",
      });
      super_hero.belongsToMany(models.mission, {
        through: models.mission_participation,
        foreignKey: "super_hero_id",
      });
      super_hero.belongsToMany(super_hero, {
        through: "companion",
        as: "companions",
        foreignKey: "super_hero_id",
      });
      super_hero.belongsToMany(super_hero, {
        through: "companion",
        as: "super_hero_companion",
        foreignKey: "companion_id",
      });
      // super_hero.hasOne(models.companion, {
      //   foreignKey: "super_hero_id",
      // });
      // super_hero.hasOne(models.companion, {
      //   foreignKey: "companion_id",
      // });

      // super_hero.belongsToMany(super_hero, {
      //   through: models.companion,
      //   // as: "super_hero",
      //   foreignKey: "super_hero_id",
      // });
      //   SuperHero.belongsToMany(SuperHero, {
      //     through: models.Companion,
      //     as: "Companion",
      //     foreignKey: "Companion_id",
      //   });
      // super_hero.belongsToMany(models.companion, {
      //   through: models.companion,
      //   foreignKey: "companion_id",
      // });
    }
  }
  super_hero.init(
    {
      name: DataTypes.STRING,
      appearence_date: DataTypes.DATE,
      death_date: DataTypes.DATE,
    },
    {
      sequelize,
      createdAt: false,
      updatedAt: false,
      modelName: "super_hero",
    }
  );
  return super_hero;
};
