"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class mission_participation extends Model {
    static associate(models) {}
  }
  mission_participation.init(
    {
      mission_id: DataTypes.INTEGER,
      super_hero_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      createdAt: false,
      updatedAt: false,
      modelName: "mission_participation",
    }
  );
  return mission_participation;
};
