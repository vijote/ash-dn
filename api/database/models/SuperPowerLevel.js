// exports a function

("use strict");
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class super_power_level extends Model {
    static associate(models) {}
  }
  super_power_level.init(
    {
      super_power_id: DataTypes.INTEGER,
      super_hero_id: DataTypes.INTEGER,
      super_power_level: DataTypes.DECIMAL(10, 1),
    },
    {
      sequelize,
      createdAt: false,
      updatedAt: false,
      modelName: "super_power_level",
    }
  );
  return super_power_level;
};
