"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class planet extends Model {
    static associate(models) {
      planet.hasOne(models.super_hero, {
        foreignKey: "home_planet",
      });
    }
  }
  planet.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      createdAt: false,
      updatedAt: false,
      modelName: "planet",
    }
  );
  return planet;
};
