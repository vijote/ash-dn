"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("super_hero", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: Sequelize.STRING,
      appearence_date: Sequelize.DATE,
      inmortal: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      death_date: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      home_planet: Sequelize.INTEGER,
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     */
    await queryInterface.dropTable("super_hero");
  },
};
