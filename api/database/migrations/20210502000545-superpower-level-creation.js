"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("super_power_level", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      super_hero_id: Sequelize.INTEGER,
      super_power_id: Sequelize.INTEGER,
      super_power_level: Sequelize.DECIMAL(10, 2),
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("super_power_level");
  },
};
