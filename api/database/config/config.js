require("dotenv").config();
module.exports = {
  development: {
    dialect: "postgres",
    define: {
      freezeTableName: true,
    },
    protocol: "postgres",
    use_env_variable: "DB_CONN_STRING",
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
  test: {
    dialect: "postgres",
    use_env_variable: "DB_CONN_STRING",
    define: {
      freezeTableName: true,
    },
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
  production: {
    dialect: "postgres",
    use_env_variable: "DB_CONN_STRING",
    define: {
      freezeTableName: true,
    },
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
