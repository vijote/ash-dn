const { sequelize } = require("../database/models");
const { super_hero, super_power_level, companion, super_power, planet } =
  sequelize.models;

module.exports = class SuperHeroController {
  static async createHero(req, res) {
    console.log(req.body);
    if (req.body.super_power_id === "no") req.body.planet_id = 3;
    if (req.body.inmortal === "si") req.body.inmortal = true;
    else req.body.inmortal = false;
    try {
      const newHero = await super_hero.create({
        name: req.body.name,
        appearence_date: req.body.appearence_date,
        inmortal: req.body.inmortal,
        home_planet: req.body.planet_id,
      });

      let promisesArray = [];
      for (let power of req.body.super_powers) {
        promisesArray.push(
          super_power_level.create({
            super_power_id: power.super_poder_id,
            super_hero_id: newHero.dataValues.id,
            super_power_level: parseFloat(power.super_poder_level),
          })
        );
      }
      if (req.body.has_companion === "si") {
        for (let companion_hero of req.body.companions) {
          console.log(companion_hero);
          promisesArray.push(
            companion.create({
              super_hero_id: newHero.dataValues.id,
              companion_id: companion_hero.super_hero_id,
            })
          );
        }
      }

      await Promise.all(promisesArray);

      console.log(newHero.dataValues);
      res.json({ message: "salio bien padre" });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }
  static async getAllHeroes(req, res) {
    try {
      const allHeroes = await super_hero.findAll({
        attributes: ["id", "name"],
      });

      res.json({ super_heroes: allHeroes });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }
  static async getHeroDetails(req, res) {
    try {
      const foundHero = await super_hero.findOne({
        where: {
          id: req.params.id,
        },
        include: [
          {
            model: super_power,
            attributes: ["name", "id"],
          },
          {
            model: super_hero,
            as: "companions",
            attributes: ["name", "id"],
          },
        ],
      });
      const foundPlanet = await planet.findOne({
        where: {
          id: foundHero.dataValues.home_planet,
        },
        attributes: ["name"],
      });

      let hero = {
        ...foundHero.dataValues,
        home_planet: foundPlanet.dataValues.name,
      };

      res.json({
        super_hero: hero,
      });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }

  static async confirmDeath(req, res) {
    console.log(req.params.id);
    try {
      const allHeroes = await super_hero.update(
        { death_date: new Date() },
        { where: { id: parseInt(req.params.id) } }
      );

      res.json({ super_heroes: allHeroes });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }
};
