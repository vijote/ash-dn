const { sequelize } = require("../database/models");
const { mission, mission_participation, super_hero } = sequelize.models;

module.exports = class MissionController {
  static async createMission(req, res) {
    console.log(req.body);
    if (req.body.type === "no") req.body.planet_id = 3;
    if (req.body.type === "mr") req.body.type = true;
    else req.body.type = false;
    try {
      const newMission = await mission.create({
        name: req.body.name,
        completion_date: req.body.realization_date,
        type: req.body.type,
        description: req.body.description,
      });

      let promisesArray = [];
      for (let heroe of req.body.heroes) {
        promisesArray.push(
          mission_participation.create({
            super_hero_id: heroe.super_hero_id,
            mission_id: newMission.dataValues.id,
          })
        );
      }

      await Promise.all(promisesArray);

      console.log(newMission.dataValues);
      res.json({ message: "salio bien padre" });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }
  static async getRecentMissions(req, res) {
    try {
      const allMissions = await mission.findAll({
        attributes: ["id", "name"],
        limit: 4,
        order: [["id", "DESC"]],
      });

      res.json({ missions: allMissions });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }

  static async getPaginatedMissions(req, res) {
    console.log(parseInt(req.params.pageNumber));
    try {
      const allMissions = await mission.findAndCountAll({
        attributes: ["id", "name"],
        ...paginate({
          page: parseInt(req.params.pageNumber),
          pageSize: 4,
        }),
        order: [["id", "DESC"]],
      });

      console.log(allMissions);

      res.json({ allMissions });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }

  static async getMissionDetails(req, res) {
    console.log(req.params);
    try {
      const missionDetails = await mission.findOne({
        where: {
          id: parseInt(req.params.missionId),
        },
        // include: [{ model: super_hero }],
        include: [
          {
            model: super_hero,
            attributes: ["name", "id"],
          },
        ],
      });
      //   const participants = await mission.findAll({
      //     where: {
      //       mission_id: missionDetails.dataValues.id,
      //     },
      //     include: [{ model: super_hero }],
      //   });

      console.log(missionDetails);

      res.json({ missionDetails });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }
};

const paginate = ({ page, pageSize }) => {
  const offset = page === 1 ? 0 : page * pageSize - pageSize;
  const limit = pageSize;

  return {
    offset,
    limit,
  };
};
