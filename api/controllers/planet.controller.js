const { sequelize } = require("../database/models");
const { planet } = sequelize.models;

module.exports = class PlanetController {
  static async createPlanet(req, res) {
    try {
      await planet.create({
        name: req.body.name,
      });

      res.json({ message: "salio bien padre" });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }

  static async getAllPlanets(req, res) {
    try {
      const allPlanets = await planet.findAll();

      res.json({ planets: allPlanets });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }
};
