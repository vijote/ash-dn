const { sequelize } = require("../database/models");
const { companion } = sequelize.models;

module.exports = class CompanionController {
  static async createCompanion(req, res) {
    try {
      await companion.create({
        super_hero_id: req.body.super_hero_id,
        companion_id: req.body.companion_id,
      });

      res.json({ message: "salio bien padre" });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }
};
