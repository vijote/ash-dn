const { sequelize } = require("../database/models");
const { super_power, super_power_level } = sequelize.models;

module.exports = class SuperPowerController {
  static async createSuperPower(req, res) {
    try {
      await super_power.create({
        name: req.body.name,
        // description: req.body.description,
      });

      res.json({ message: "salio bien padre" });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }

  static async createSuperPowerLevel(req, res) {
    try {
      await super_power_level.create({
        super_power_id: req.body.super_power_id,
        super_hero_id: req.body.super_hero_id,
        super_power_level: req.body.super_power_level,
      });

      res.json({ message: "salio bien padre" });
    } catch (error) {
      console.log(error);
      res.json({ error: "salio mal rey" });
    }
  }

  static async getAllPowers(req, res) {
    try {
      const allPowers = await super_power.findAll();

      res.json({ super_powers: allPowers });
    } catch (error) {
      console.log(error);
    }
  }
};
