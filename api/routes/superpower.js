const express = require("express");
const router = express.Router();
const {
  createSuperPower,
  createSuperPowerLevel,
  getAllPowers,
} = require("../controllers/superpower.controller");

// POST /api/superpower
router.route("/").post(createSuperPower);
router.route("/level").post(createSuperPowerLevel);
router.route("/").get(getAllPowers);

module.exports = router;
