const express = require("express");
const router = express.Router();
const {
  createMission,
  getRecentMissions,
  getPaginatedMissions,
  getMissionDetails,
} = require("../controllers/mission.controller");

// POST /api/mission
router.route("/").post(createMission);
router.route("/").get(getRecentMissions);
router.route("/page/:pageNumber").get(getPaginatedMissions);
router.route("/:missionId").get(getMissionDetails);

module.exports = router;
