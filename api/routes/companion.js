const express = require("express");
const router = express.Router();
const { createCompanion } = require("../controllers/companion.controller");

// POST /api/companion
router.route("/").post(createCompanion);

module.exports = router;
