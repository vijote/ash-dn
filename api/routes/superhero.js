const express = require("express");
const router = express.Router();
const {
  createHero,
  getAllHeroes,
  getHeroDetails,
  confirmDeath,
} = require("../controllers/superhero.controller");

// POST /api/superhero
router.route("/").post(createHero);
router.route("/").get(getAllHeroes);
router.route("/:id").get(getHeroDetails);
router.route("/death/:id").get(confirmDeath);

module.exports = router;
