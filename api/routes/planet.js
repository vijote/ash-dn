const express = require("express");
const router = express.Router();
const {
  createPlanet,
  getAllPlanets,
} = require("../controllers/planet.controller");

// POST /api/planet
router.route("/").post(createPlanet);
router.route("/").get(getAllPlanets);

module.exports = router;
